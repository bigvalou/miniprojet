import string

def clean_and_return(a):
    a=string.strip(a)
    a=string.lower(a)
    for x in string.whitespace[:-1]:
        a=a.replace(x,'')
    for x in string.punctuation[:-1]:
        a=a.replace(x,'')
    for x in a:
        if x>'z':
            a=a.replace(x,'')
    for x in string.digits:
        a=a.replace(x,'')
    b=a.split()
    return b

def markov(a):
    f=open('./'+a,'r')
    d=dict()
    a='word'
    b=''
    while a != '':
        a=f.readline()
        b+=a
    a=clean_and_return(b)
    for i in range(len(a)-3):
        if d.get((a[i],a[i+1]),[])==[]:
            d[(a[i],a[i+1])]=[]
        d[(a[i],a[i+1])].append([a[i+2],a[i+3]])
    f.close()
    return d
