﻿"""Exercise I.1. Write a program that reads a ﬁle, breaks each line into words,
strips white space and punctuation from the words,
and converts them to lowercase. (10 points)"""

import sys
import string
import re
def nettoyeurFichier(document):
    
    with open(document) as document:
        
            listeDesMots = []    
            for line in document.readlines():
                enlevement_espace = line.strip()  
                sectionner = enlevement_espace.split()  
                for enlevement_Conversion in sectionner:
                   
                   enlevement_Conversion = enlevement_Conversion.translate(string.maketrans("" , ""), string.punctuation).lower()    
                   listeDesMots = listeDesMots + [enlevement_Conversion]
            return listeDesMots       
            

