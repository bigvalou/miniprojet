"""Exercise I.3. Modify the program from the previous
exercise to print the 20 most frequently-used words in the book. (10 points)"""




from collections import Counter
import re
import string
def exotrois(doc):
    with open(doc) as doc:
        l= []
        numArray = []
        readDoc  = doc.read()
        readDoc1 = readDoc.translate(string.maketrans("" , ""), string.punctuation).lower()
        readDoc2  = readDoc1.replace(" ","\n")
        readDoc4= readDoc2.split()
        occurenceDesMots = Counter(readDoc4).most_common(20)
        print "The top twenty most common words are: ",occurenceDesMots
    
