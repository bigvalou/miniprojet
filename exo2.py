﻿"""Exercise I.2. Go to Project Gutenberg (http://gutenberg.org) and download your favorite out-of-copyright book in plain text format.
Modify your program from the previous exercise to read the book you downloaded, skip over the header information at the beginning of the ﬁle, and process the rest of the words as before.
Then modify the program to count the total number of words in the book, and the number of times each word is used.
Print the number of different words used in the book. Compare different books by different authors, written in different eras. Which author uses the most extensive vocabulary?  (15 points)"""




from collections import Counter
import string
differentMots2= 0
totalWords = 0

def exo2(doc):
    with open(doc) as doc:

        startBook = False
        l= []
        readDoc  = doc.read()
        readDoc1 = readDoc.translate(string.maketrans("" , ""), string.punctuation).lower()
        readDoc2  = readDoc1.replace(" ","\n")
        readDoc4= readDoc2.split()
        occurenceDesMots = Counter(readDoc4)
        differentMots = len(occurenceDesMots)
        print "Le nombre de mots total: {} \t\n L'occurence des mots {}\t\n Nombre de mots differents: \t\n {}". format(len(do4),occurenceDesMots,differentMots)

    totalWords = 0
    fullArray = []
    with open("book1.txt","r") as document:
        for line in document.readlines():
            if line.find("*** START OF THIS PROJECT GUTENBERG EBOOK") != -1:
                startBook = True
            elif line.find("*** END OF THIS PROJECT GUTENBERG EBOOK") != -1: 
                startBook = False       
            elif startBook and line.find("*** START OF THIS PROJECT GUTENBERG EBOOK") == -1 and len(line) > 1:
                cleanedLine = line.strip()
                words = cleanedLine.split()
                modifiedWords = []
                for word in words:
                    word = word.translate(string.maketrans("",""), string.punctuation).lower()
                    if word != "":
                        modifiedWords = modifiedWords + [word]
                        totalWords = totalWords + 1
                    fullArray = fullArray + modifiedWords                
                
                    global differentMots2
                occurenceDesMots2 = Counter(fullArray)    
                differentMots2 = len(occurenceDesMots2)
                print "{}\n{}". format(occurenceDesMots2,differentMots2)
    if differentMots > differentMots2:
        print "{}". format("Le livre velo.txt est plus riche en vocab"), differentMots

    else :
        print ("gutenberg est plus riche en vocab"), differentMots2
