# -*-coding:Latin-1 -*

"""
Exercice VI.1
"""
import sys, string

from collections import OrderedDict

def traitementDeLigne(chainededans, mettrededans):
    """On ajoute chaque mot and le compte pour chaque ligne"""
    ligne = chainededans.strip()
    mots = ligne.split()
    for mot in mots:
        mot = mot.translate(string.maketrans("",""), string.punctuation + string.digits).lower()
        mettrededans.add(mot)
    return mettrededans

def traitementDeFichier(fin, processFunc):
    """Renvoie un set de mots distincts"""
    s = set()    
    try:
        monFichier = open(fin, 'r')
        for ligne in monFichier.readlines():
            processFunc(ligne, s)
        return s
    except:
        e = sys.exc_info()
        print e
    finally:
        monFichier.close()
        
if __name__ == '__main__':
    fichierLivre = 'fichier.txt'
    fichierMot = 'word.txt'
    livreMot = traitementDeFichier(fichierLivre, traitementDeLigne)
    motMots = traitementDeFichier(fichierMot, traitementDeLigne)
    # See for more set methods http://docs.python.org/2/library/stdtypes.html#set
    motsEtrangesLivre = livreMot.difference(motMots)
    print motsEtrangesLivre
    
